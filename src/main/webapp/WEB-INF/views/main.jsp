<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product Manager</title>
</head>
<body>
<%@include file="authheader.jsp" %>
Go to <a href="<c:url value='/list_products' />">List of All Products</a>
<br/>
Go to <a href="<c:url value='/list_manufacturers' />">List of All Manufacturers</a>
</body>
</html>
