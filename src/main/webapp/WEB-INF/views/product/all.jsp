<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Product Manager</title>
</head>
<body>
<h2>List of Products</h2>
<table>
    <tr>
        <td>NAME</td>
        <sec:authorize access="hasRole('ADMIN')">
            <td></td>
            <td></td>
        </sec:authorize>
    </tr>
    <c:forEach items="${products}" var="product">
        <tr>
            <td><a href="<c:url value='/show-${product.id}-product' />">${product.name}</a></td>
            <sec:authorize access="hasRole('ADMIN')">
                <td><a href="<c:url value='/edit-${product.id}-product' />">update</a></td>
                <td><a href="<c:url value='/delete-${product.id}-product' />">delete</a></td>
            </sec:authorize>
        </tr>
    </c:forEach>
</table>
<br/>
<sec:authorize access="hasRole('ADMIN')">
    <a href="<c:url value="/new_product" />">Add New Product</a>
</sec:authorize>
<br/>
<br/>
Go back to <a href="<c:url value='/' />">main</a>
</body>
</html>
