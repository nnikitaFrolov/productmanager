<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Product Manager</title>
</head>
<body>
<h2>Product</h2>
<table>
    <tr>
        <td>ID</td>
        <td>NAME</td>
        <td>PRISE</td>
        <td>DESCRIPTION</td>
        <td>MANUFACTURER</td>
    </tr>
    <tr>
        <td>${product.id}</td>
        <td>${product.name}</td>
        <td>${product.price}</td>
        <td>${product.description}</td>
        <td>${product.manufacturer.name}</td>
    </tr>
</table>
<br/>
Go back to <a href="<c:url value='/list_products' />">List of All Products</a>
</body>
</html>