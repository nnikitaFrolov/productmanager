<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>Login page</title>
	</head>

	<body>
		<c:url var="loginUrl" value="/login" />
		<form action="${loginUrl}" method="post">
			<c:if test="${param.error != null}">
				<div>
					<p>Invalid username and password.</p>
				</div>
			</c:if>
			<c:if test="${param.logout != null}">
				<div>
					<p>You have been logged out successfully.</p>
				</div>
			</c:if>
			<div>
				<label for="username"><i></i></label>
				<input type="text" id="username" name="ssoId" placeholder="Enter Username" required>
			</div>
			<div>
				<label for="password"><i></i></label>
				<input type="password" id="password" name="password" placeholder="Enter Password" required>
			</div>
			<div>
			  <div>
				<label><input type="checkbox" id="rememberme" name="remember-me"> Remember Me</label>
			  </div>
			</div>
			<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />

			<div>
				<input type="submit" value="Log in">
			</div>
		</form>

	</body>
</html>