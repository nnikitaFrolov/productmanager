<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Product Manager</title>
</head>
<body>
<h2>Registration Form</h2>
<form:form method="POST" modelAttribute="manufacturer">
    <table>
        <tr>
            <td><label for="name">Name: </label></td>
            <td><form:input path="name" id="name"/></td>
            <td><form:errors path="name" cssClass="error"/></td>
        </tr>

        <tr>
            <td><label for="address">Address: </label></td>
            <td><form:input path="address" id="address"/></td>
            <td><form:errors path="address" cssClass="error"/> </td>
        </tr>

        <tr>
            <td><label for="description">Description: </label></td>
            <td><form:textarea path="description" id="description" rows="5" cols="20"/></td>
            <td><form:errors path="description" cssClass="error"/> </td>
        </tr>

        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Register"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
<br/>
<br/>
Go back to <a href="<c:url value='/list_manufacturers' />">List of All Products</a>
</body>
</html>
