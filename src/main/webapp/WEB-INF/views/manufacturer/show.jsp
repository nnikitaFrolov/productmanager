<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Product Manager</title>
</head>
<body>
<h2>Manufacturer</h2>
<table>
    <tr>
        <td>ID</td>
        <td>NAME</td>
        <td>ADDRESS</td>
        <td>DESCRIPTION</td>
        <td>PRODUCTS</td>
    </tr>
    <tr>
        <td>${manufacturer.id}</td>
        <td>${manufacturer.name}</td>
        <td>${manufacturer.address}</td>
        <td>${manufacturer.description}</td>
    </tr>
</table>
<ul>
    <c:forEach var="product" items="${products}">
        <li>${product.name}</li>
    </c:forEach>
</ul>
<br/>
Go back to <a href="<c:url value='/list_manufacturers' />">List of All Manufacturer</a>
</body>
</html>