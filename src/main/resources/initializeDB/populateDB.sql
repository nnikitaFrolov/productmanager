INSERT INTO manufacturers (name, address, description) VALUES ('Apple', 'California', 'company');
INSERT INTO manufacturers (name, address, description) VALUES ('LG', 'Seul', 'company');
INSERT INTO products (name, price, description, id_manufacturer) VALUES ('IPhone', 400, 'phone', 1);
INSERT INTO products (name, price, description, id_manufacturer) VALUES ('LG', 300, 'phone', 2);
