package com.nikitafrolov.service;

import com.nikitafrolov.model.Manufacturer;
import com.nikitafrolov.model.Product;

import java.util.List;

public interface ManufacturerService {

    Manufacturer getById(Long key);

    void save(Manufacturer manufacturer);

    void update(Manufacturer manufacturer);

    void remove(Long key);

    List<Manufacturer> getAll();

    void addProduct(Long id, Product product);
}
