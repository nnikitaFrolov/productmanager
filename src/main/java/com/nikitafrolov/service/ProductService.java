package com.nikitafrolov.service;

import com.nikitafrolov.model.Product;

import java.util.List;

public interface ProductService {

    Product getById(Long key);

    void save(Product product);

    void update(Product product);

    void remove(Long key);

    List<Product> getAll();
}
