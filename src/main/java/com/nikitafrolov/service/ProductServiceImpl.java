package com.nikitafrolov.service;

import com.nikitafrolov.dao.ProductDao;
import com.nikitafrolov.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService{

    @Autowired
    private ProductDao dao;

    public Product getById(Long key) {
        return dao.findById(key);
    }

    public void save(Product product) {
        dao.saveProduct(product);
    }

    public void update(Product product) {
        dao.updateProduct(product);
    }

    public void remove(Long key) {
        dao.deleteProduct(key);
    }

    public List<Product> getAll() {
        return dao.findAllProduct();
    }
}
