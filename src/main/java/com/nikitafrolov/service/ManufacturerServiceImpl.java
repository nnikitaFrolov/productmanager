package com.nikitafrolov.service;

import com.nikitafrolov.dao.ManufacturerDao;
import com.nikitafrolov.model.Manufacturer;
import com.nikitafrolov.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("manufacturerService")
@Transactional
public class ManufacturerServiceImpl implements ManufacturerService{

    @Autowired
    private ManufacturerDao dao;

    @Transactional
    public Manufacturer getById(Long key) {
        return dao.findById(key);
    }

    @Transactional
    public void save(Manufacturer manufacturer) {
        dao.saveManufacturer(manufacturer);
    }

    @Transactional
    public void update(Manufacturer manufacturer) {
        dao.updateManufacturer(manufacturer);
    }

    @Transactional
    public void remove(Long key) {
        dao.deleteManufacturer(key);
    }

    @Transactional
    public List<Manufacturer> getAll() {
        return dao.findAllManufacturer();
    }

    @Transactional
    public void addProduct(Long id, Product product){
        dao.addProduct(id, product);
    }
}
