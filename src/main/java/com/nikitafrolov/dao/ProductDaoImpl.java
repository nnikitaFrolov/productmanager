package com.nikitafrolov.dao;

import com.nikitafrolov.model.Product;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao<Long, Product> implements ProductDao {
    private static final Logger logger = LoggerFactory.getLogger(ProductDaoImpl.class);

    public Product findById(Long id) {
        Product product = getByKey(id);
        logger.info("Product successfully loaded. Product details: " + product);
        return product;
    }

    public void updateProduct(Product product) {
        update(product);
        logger.info("Product successfully update. Product details: " + product);
    }

    public void saveProduct(Product product) {
        persist(product);
        logger.info("Product successfully save. Product details: " + product);
    }

    public void deleteProduct(Long id) {
        Session session = getSession();
        Product product = session.load(Product.class, id);

        if (product != null) {
            session.delete(product);
            logger.info("Product successfully removed. Product details: " + product);
        }else{
            logger.info("Product fail removed. Product details: null");
        }
    }

    @SuppressWarnings("unchecked")
    public List<Product> findAllProduct() {
        List<Product> products = getSession().createQuery("from Product").list();
        for (Product product : products) {
            logger.info("Product: " + product);
        }
        return products;
    }
}
