package com.nikitafrolov.dao;

import com.nikitafrolov.model.User;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao{

    static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    public User findById(int id) {
        User user = getByKey(id);
        if(user!=null){
            Hibernate.initialize(user.getUserProfiles());
        }
        return user;
    }

    @Override
    public User findBySSO(String sso) {
        logger.info("SSO : {}", sso);
        User user = (User)getSession().createQuery("from User where  ssoId = '" + sso + "'").uniqueResult();
        if(user!=null){
            Hibernate.initialize(user.getUserProfiles()); //TODO make this for Manufacturer
        }
        return user;
    }

    @Override
    public void save(User user) {
        persist(user);
    }

    @Override
    public void deleteBySSO(String sso) {
        User user = (User)getSession().createQuery("from User where  ssoId = '" + sso + "'").uniqueResult();
        delete(user);
    }

    @Override
    public List<User> findAllUsers() {
        return getSession()
                .createQuery("select distinct u from User u order by ssoId asc", User.class).list();
    }
}
