package com.nikitafrolov.dao;

import com.nikitafrolov.model.Manufacturer;
import com.nikitafrolov.model.Product;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("manufacturerDao")
public class ManufacturerDaoImpl extends AbstractDao<Long, Manufacturer> implements ManufacturerDao{
    private static final Logger logger = LoggerFactory.getLogger(ManufacturerDaoImpl.class);

    public Manufacturer findById(Long id) {
        Manufacturer manufacturer = getByKey(id);
        logger.info("Manufacturer successfully loaded. Manufacturer details: " + manufacturer);
        return manufacturer;
    }

    public void updateManufacturer(Manufacturer manufacturer) {
        update(manufacturer);
        logger.info("Manufacturer successfully update. Manufacturer details: " + manufacturer);
    }

    public void saveManufacturer(Manufacturer manufacturer) {
        persist(manufacturer);

        logger.info("Manufacturer successfully save. Manufacturer details: " + manufacturer);
    }

    public void deleteManufacturer(Long id) {
        Session session = getSession();
        Manufacturer manufacturer = session.load(Manufacturer.class, id);

        if (manufacturer != null) {
            session.delete(manufacturer);
            logger.info("Manufacturer successfully removed. Manufacturer details: " + manufacturer);
        }else {
            logger.info("Manufacturer fail removed. Manufacturer details: null");
        }
    }

    @SuppressWarnings("unchecked")
    public List<Manufacturer> findAllManufacturer() {
        List<Manufacturer> manufacturers = getSession().createQuery("from Manufacturer").list();
        for (Manufacturer manufacturer : manufacturers) {
            logger.info("Manufacturer: " + manufacturer);
        }
        return manufacturers;
    }

    public void addProduct(Long id, Product product){
        Session session = getSession();
        Manufacturer manufacturer = session.load(Manufacturer.class, id);
        manufacturer.getProducts().add(product);
        session.saveOrUpdate(product);
        logger.info("Product successfully update. Product details: " + product);
        session.saveOrUpdate(manufacturer);
        logger.info("Manufacturer successfully update. Manufacturer details: " + manufacturer);
    }
}
