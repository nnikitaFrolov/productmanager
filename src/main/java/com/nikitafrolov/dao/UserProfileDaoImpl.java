package com.nikitafrolov.dao;

import com.nikitafrolov.model.UserProfile;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userProfileDao")
public class UserProfileDaoImpl extends AbstractDao<Integer, UserProfile>implements UserProfileDao{
    @Override
    public List<UserProfile> findAll() {
        return getSession()
                .createQuery("from UserProfile order by type asc", UserProfile.class).list();
    }

    @Override
    public UserProfile findByType(String type) {
        return getSession()
                .createQuery("from UserProfile where  type = '" + type + "'", UserProfile.class).uniqueResult();
    }

    @Override
    public UserProfile findById(int id) {
        return getByKey(id);
    }
}
