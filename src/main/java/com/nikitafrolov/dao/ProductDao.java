package com.nikitafrolov.dao;

import com.nikitafrolov.model.Product;

import java.util.List;

public interface ProductDao {

    Product findById(Long id);

    void updateProduct(Product product);

    void saveProduct(Product product);

    void deleteProduct(Long id);

    List<Product> findAllProduct();
}
