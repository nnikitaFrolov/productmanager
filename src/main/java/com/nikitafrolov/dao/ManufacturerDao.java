package com.nikitafrolov.dao;

import com.nikitafrolov.model.Manufacturer;
import com.nikitafrolov.model.Product;

import java.util.List;

public interface ManufacturerDao {

    Manufacturer findById(Long id);

    void updateManufacturer(Manufacturer manufacturer);

    void saveManufacturer(Manufacturer manufacturer);

    void deleteManufacturer(Long id);

    List<Manufacturer> findAllManufacturer();

    void addProduct(Long id, Product product);
}
