package com.nikitafrolov.controller;

import com.nikitafrolov.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;

import java.beans.PropertyEditorSupport;


public class EditorManufacturer extends PropertyEditorSupport {
    private ManufacturerService manufacturerService;

    public EditorManufacturer (ManufacturerService manufacturerService){
        this.manufacturerService = manufacturerService;
    }

    @Override
    public void setAsText(String key) throws IllegalArgumentException {
        if (key.equals("NONE")) {
            this.setValue(null);
        } else {
            this.setValue(manufacturerService.getById(Long.parseLong(key)));
        }
    }
}
