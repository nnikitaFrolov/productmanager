package com.nikitafrolov.controller;

import com.nikitafrolov.model.Manufacturer;
import com.nikitafrolov.model.Product;
import com.nikitafrolov.model.User;
import com.nikitafrolov.service.ManufacturerService;
import com.nikitafrolov.service.ProductService;
import com.nikitafrolov.service.UserProfileService;
import com.nikitafrolov.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;


@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class AppController {

    private static final Logger logger = LoggerFactory.getLogger(EditorManufacturer.class);

    @Autowired
    private ProductService productService;

    @Autowired
    private ManufacturerService manufacturerService;

    @Autowired
    UserService userService;

    @Autowired
    UserProfileService userProfileService;

    @Autowired
    MessageSource messageSource;

    @Autowired
    PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

    @Autowired
    AuthenticationTrustResolver authenticationTrustResolver;

    /**
     * This method will list products and manufacturer.
     */
//    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
//    public String main() {
//        return "main";
//    }

    /**
     * This method will list all existing users.
     */
    @RequestMapping(value = { "/" }, method = RequestMethod.GET)
    public String listUsers(ModelMap model) {

        List<User> users = userService.findAllUsers();
        model.addAttribute("users", users);
        model.addAttribute("loggedinuser", getPrincipal());
        return "main";
    }

    /**
     * This method handles login GET requests.
     * If users is already logged-in and tries to goto login page again, will be redirected to list page.
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        if (isCurrentAuthenticationAnonymous()) {
            return "login";
        } else {
            return "redirect:/list";
        }
    }

    /**
     * This method handles logout requests.
     * Toggle the handlers if you are RememberMe functionality is useless in your app.
     */
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            //new SecurityContextLogoutHandler().logout(request, response, auth);
            persistentTokenBasedRememberMeServices.logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/login?logout";
    }

    /**
     * This method will list all existing products.
     */
    @RequestMapping(value = {"/list_products"}, method = RequestMethod.GET)
    public String listProducts(ModelMap model) {
        List<Product> products = productService.getAll();
        model.addAttribute("products", products);
        model.addAttribute("loggedinuser", getPrincipal());
        return "product/all";
    }

    /**
     * This method will list all existing manufacturers.
     */
    @RequestMapping(value = {"/list_manufacturers"}, method = RequestMethod.GET)
    public String listManufacturer(ModelMap model) {
        List<Manufacturer> manufacturers = manufacturerService.getAll();
        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("loggedinuser", getPrincipal());
        return "manufacturer/all";
    }

    /**
     * This method will provide the medium to add a new product.
     */
    @RequestMapping(value = {"/new_product"}, method = RequestMethod.GET)
    public String newProduct(ModelMap model) {
        Product product = new Product();
        List<Manufacturer> manufacturers = manufacturerService.getAll();
        model.addAttribute("product", product);
        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("edit", false);
        return "product/registration";
    }

    /**
     * This method will provide the medium to add a new manufacturer.
     */
    @RequestMapping(value = {"/new_manufacturer"}, method = RequestMethod.GET)
    public String newManufacturer(ModelMap model) {
        Manufacturer manufacturer = new Manufacturer();
        model.addAttribute("manufacturer", manufacturer);
        model.addAttribute("edit", false);
        return "manufacturer/registration";
    }

    /**
     * This method will be called on form submission, handling POST request for
     * saving product in database. It also validates the user input
     */
    @RequestMapping(value = {"/new_product"}, method = RequestMethod.POST)
    public String saveProduct(@Valid Product product, BindingResult result,
                               ModelMap model) {

        if (result.hasErrors()) {
            return "product/registration";
        }

        productService.save(product);
        Product persistProduct = productService.getById(product.getId());
        Manufacturer manufacturer = manufacturerService.getById(persistProduct.getManufacturer().getId());
        manufacturer.getProducts().add(persistProduct);
        manufacturerService.update(manufacturer);
        //manufacturerService.addProduct(product.getManufacturer().getId(), persistProduct);

        model.addAttribute("success", "Product " + product.getName()
                + " registered successfully");
        return "product/success";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Manufacturer.class, new EditorManufacturer(manufacturerService));
    }

    /**
     * This method will be called on form submission, handling POST request for
     * saving manufacturer in database. It also validates the user input
     */
    @RequestMapping(value = {"/new_manufacturer"}, method = RequestMethod.POST)
    public String saveManufacturer(@Valid Manufacturer manufacturer, BindingResult result,
                              ModelMap model) {

        if (result.hasErrors()) {
            return "manufacturer/registration";
        }

        manufacturerService.save(manufacturer);

        model.addAttribute("success", "Manufacturer " + manufacturer.getName()
                + " registered successfully");
        return "manufacturer/success";
    }

	/**
     * This method will provide the medium to update an existing product.
	 */
    @RequestMapping(value = { "/edit-{id}-product" }, method = RequestMethod.GET)
    public String editProduct(@PathVariable Long id, ModelMap model) {
		Product product = productService.getById(id);
        List<Manufacturer> manufacturers = manufacturerService.getAll();
		model.addAttribute("product", product);
        model.addAttribute("manufacturers", manufacturers);
		model.addAttribute("edit", true);
		return "product/registration";
	}

    /**
     * This method will provide the medium to update an existing manufacturer.
     */
    @RequestMapping(value = { "/edit-{id}-manufacturer" }, method = RequestMethod.GET)
    public String editManufacturer(@PathVariable Long id, ModelMap model) {
        Manufacturer manufacturer = manufacturerService.getById(id);
        model.addAttribute("manufacturer", manufacturer);
        model.addAttribute("edit", true);
        return "manufacturer/registration";
    }

	/**
	 * This method will be called on form submission, handling POST request for
	 * updating product in database. It also validates the user input
	 */
    @RequestMapping(value = { "/edit-{id}-product" }, method = RequestMethod.POST)
    public String updateProduct(@Valid Product product, BindingResult result,
			ModelMap model) {

		if (result.hasErrors()) {
			return "product/registration";
		}

        productService.update(product);

		model.addAttribute("success", "Product " + product.getName()
                + " updated successfully");
		return "product/success";
	}

    /**
     * This method will be called on form submission, handling POST request for
     * updating manufacturer in database. It also validates the user input
     */
    @RequestMapping(value = { "/edit-{id}-manufacturer" }, method = RequestMethod.POST)
    public String updateManufacturer(@Valid Manufacturer manufacturer, BindingResult result,
                                ModelMap model) {

        if (result.hasErrors()) {
            return "manufacturer/registration";
        }

        manufacturerService.update(manufacturer);

        model.addAttribute("success", "Manufacturer " + manufacturer.getName()
                + " updated successfully");
        return "manufacturer/success";
    }

	/**
	 * This method will delete an product by it's ID value.
	 */
    @RequestMapping(value = { "/delete-{id}-product" }, method = RequestMethod.GET)
	public String deleteProduct(@PathVariable Long id) {
		productService.remove(id);
		return "redirect:/list_products";
	}

    /**
     * This method will delete an manufacturer by it's ID value.
     */
    @RequestMapping(value = { "/delete-{id}-manufacturer" }, method = RequestMethod.GET)
    public String deleteManufacturer(@PathVariable Long id) {
        manufacturerService.remove(id);
        return "redirect:/list_manufacturers";
    }

    /**
     * This method will show an product by it's ID value.
     */
    @RequestMapping(value = { "/show-{id}-product" }, method = RequestMethod.GET)
    public String showProduct(@PathVariable Long id, ModelMap model) {
        Product product = productService.getById(id);
        model.addAttribute("product", product);
        return "product/show";
    }

    /**
     * This method will show an manufacturer by it's ID value.
     */
    @RequestMapping(value = { "/show-{id}-manufacturer" }, method = RequestMethod.GET)
    public String showManufacturer(@PathVariable Long id, ModelMap model) {
        Manufacturer manufacturer = manufacturerService.getById(id);
        List<Product> products = manufacturer.getProducts();
        logger.error(products.toString());
        model.addAttribute("manufacturer", manufacturer);
        model.addAttribute("products", products);
        return "manufacturer/show";
    }

    /**
     * This method returns the principal[user-name] of logged-in user.
     */
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    /**
     * This method returns true if users is already authenticated [logged-in], else false.
     */
    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authenticationTrustResolver.isAnonymous(authentication);
    }

    /**
     * This method handles Access-Denied redirect.
     */
    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("loggedinuser", getPrincipal());
        return "accessDenied";
    }
}
