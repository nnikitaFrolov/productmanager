package com.nikitafrolov.controller;

import com.nikitafrolov.service.ProductService;

import java.beans.PropertyEditorSupport;

public class EditorProduct extends PropertyEditorSupport {
    private ProductService productService;

    public EditorProduct(ProductService productService) {
        this.productService = productService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        setValue(productService.getById(Long.parseLong(text)));
    }
}
